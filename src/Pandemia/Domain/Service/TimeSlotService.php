<?php

namespace Kata\Pandemia\Domain\Service;

use Kata\Pandemia\Domain\Model\Person;
use Kata\Pandemia\Domain\Model\TimeSlot;

final class TimeSlotService
{
    public static function getTimeSlots(string $country): array
    {
        switch ($country) {
            case 'Spain':
                $slots = [
                    new TimeSlot(TimeSlot::SLOT_MORNING, Person::AGE_CHILD, Person::HEALTHY_STATUS),
                    new TimeSlot(TimeSlot::SLOT_NOON, Person::AGE_ADULT, Person::HEALTHY_STATUS),
                    new TimeSlot(TimeSlot::SLOT_AFTERNOON, Person::AGE_OLD, Person::RISKY_STATUS)
                ];
                break;
            default:
                $slots = [];
        }

        return $slots;
    }
}