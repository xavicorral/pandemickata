<?php

namespace Kata\Pandemia\Domain\Model;

final class Person
{
    const AGE_CHILD = 'child';
    const AGE_ADULT = 'adult';
    const AGE_OLD = 'old';

    const HEALTHY_STATUS = 'helthy';
    const RISKY_STATUS = 'risky';

    private string $age;
    private string $healthStatus;

    public function __construct(string $age, string $healthStatus)
    {
        $this->age = $age;
        $this->healthStatus = $healthStatus;
    }

    public function getAge(): string
    {
        return $this->age;
    }

    public function getHealthStatus(): string
    {
        return $this->healthStatus;
    }
}