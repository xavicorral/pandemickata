<?php

namespace Kata\Pandemia\Domain\Model;

final class TimeSlot
{
    CONST SLOT_MORNING = 'morning';
    CONST SLOT_NOON = 'noon';
    CONST SLOT_AFTERNOON = 'afternoon';

    private $slot;
    private $allowedAge;
    private $allowedHealthStatus;

    public function __construct(string $slot, string $allowedAge, string $allowedHealthStatus)
    {
        $this->slot = $slot;
        $this->allowedAge = $allowedAge;
        $this->allowedHealthStatus = $allowedHealthStatus;
    }

    public function getSlot(): string
    {
        return $this->slot;
    }

    public function getAllowedHealthStatus(): string
    {
        return $this->allowedHealthStatus;
    }

    public function getAllowedAge(): string
    {
        return $this->allowedAge;
    }
}