<?php

namespace Kata\Pandemia\Domain\Model;

final class Persons
{
    private array $persons;

    public function __construct(array $persons)
    {
        $this->persons = $persons;
    }

    public function getPersons(): array
    {
        return $this->persons;
    }

    public function addPerson(Person $person)
    {
        $this->persons[] = $person;
    }

    public function toArray()
    {
        return $this->persons;
    }
}