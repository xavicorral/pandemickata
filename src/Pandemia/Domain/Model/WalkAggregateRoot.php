<?php

namespace Kata\Pandemia\Domain\Model;

final class WalkAggregateRoot
{
    private Persons $persons;
    private TimeSlot $timeSlot;

    public function __construct(Persons $persons, TimeSlot $timeSlot)
    {
        $this->persons = $persons;
        $this->timeSlot = $timeSlot;
    }

    public function getPersons(): Persons
    {
        return $this->persons;
    }

    public function setPersons(Persons $persons): void
    {
        $this->persons = $persons;
    }

    public function getTimeSlot(): TimeSlot
    {
        return $this->timeSlot;
    }

    public function setTimeSlot(TimeSlot $timeSlot): void
    {
        $this->timeSlot = $timeSlot;
    }

    public function toArray()
    {
        return [
            $this->timeSlot->getSlot() => $this->persons->toArray()
        ];
    }
}