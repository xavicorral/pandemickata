<?php

namespace Kata;

use Kata\Pandemia\Domain\Model\Persons;
use Kata\Pandemia\Domain\Model\TimeSlot;
use Kata\Pandemia\Domain\Model\WalkAggregateRoot;
use Kata\Pandemia\Domain\Service\TimeSlotService;

class Evaluator
{
    public function evaluate(array $combination): array
    {
        $slots = TimeSlotService::getTimeSlots($combination['country']);
        $familyMembers = $combination['family'];
        $walks = [];

        foreach ($slots as $slot) {
            if ($slot->getAllowedAge() === $familyMembers[0]['age']) {
                $walks[$slot->getSlot()] = (new WalkAggregateRoot(
                    new Persons($familyMembers[0]),
                    $slot
                ))->toArray();
            }
        }
        return $walks;
    }
}