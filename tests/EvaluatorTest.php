<?php
namespace Kata\Tests;

use Kata\Evaluator;
use PHPUnit\Framework\TestCase;

class EvaluatorTest extends TestCase
{
    private $evaluator;

    protected function setUp(): void
    {
        $this->evaluator = new Evaluator();
    }

    /** @test */
    public function givenAnAdultReturnsOneNoonSlot()
    {
        $combination = [
            'country' => 'Spain',
            'family' => [
                [
                    'age'           => 'adult',
                    'health-status' => 'healthy',
                ]
            ]
        ];
        $expectedSlots = [
            'noon' => [
                [
                    [
                        'age'           => 'adult',
                        'health-status' => 'healthy',
                    ]
                ]
            ]
        ];
        $this->assertEquals($expectedSlots, $this->evaluator->evaluate($combination));
    }

    /** test */
    public function givenAnOldPersonReturnsOneAfternoonSlot()
    {
        $combination = [
            'country' => 'Spain',
            'family' => [
                [
                    'age'           => 'old',
                    'health-status' => 'healthy',
                ]
            ]
        ];
        $expectedSlots = [
            'afternoon' => [
                [
                    [
                        'age'           => 'old',
                        'health-status' => 'healthy',
                    ]
                ]
            ]
        ];
        $this->assertEquals($expectedSlots, $this->evaluator->evaluate($combination));
    }

    /**
     test
     */
    public function givenAChildAndAnAdultReturnsOneMorningSlot()
    {
        $combination = [
            'country' => 'Spain',
            'family' => [
                [
                    'age'           => 'adult',
                    'health-status' => 'healthy',
                ],
                [
                    'age'           => 'child',
                    'health-status' => 'healthy',
                ],
            ],
        ];
        $this->assertIsArray($this->evaluator->evaluate($combination));
    }
}